
var startX = -1;

$(window).load(function() {
   resizer();


   $(window).resize(function(){
      resizer();
   });

   $('body').click(function(){
      $('.wind').hide();
   });
   
   $('.langs').click(function(){
      var el = $(this).find('div').eq(0);
      if (el.hasClass('ru')){
         el.removeClass('ru');
         el.addClass('en');
      } else {
         el.addClass('ru');
         el.removeClass('en');
      }
   });

   $('.style-input').focus(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val){
         $(this).val('');

      }
      $(this).addClass('with-val');
   });

   $('.style-input').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.openwind').click(function(ev){
      ev.stopImmediatePropagation();

      $('.wind').show();
      var he = 398/2;
      $('.wind').css({"margin-top":-he+"px"});
   });

   $('.submit-but').click(function(){
      $(this).closest("form").submit();
   });



   $('.left-arr, .big-left-arr, .big-left-arr2').click(function(){
      var k = $(this).closest('.slider').find('.one-num.active');
      k = k.prev('.one-num');
      if (k.length===0){
         k = $(this).closest('.slider').find('.one-num');
         k = k.eq(k.length-1);
      }
      if (k.length>0){
         k.eq(0).trigger("click");
      }
   });

   $('.right-arr, .big-right-arr, .big-right-arr2').click(function(ev){
      ev.stopImmediatePropagation();
      var k = $(this).closest('.slider').find('.one-num.active');
      k = k.next('.one-num');
      if (k.length===0){
         k = $(this).closest('.slider').find('.one-num');
         k = k.eq(0);
      }
      if (k.length>0){
         k.eq(0).trigger("click");
      }
   });

   $(window).scroll(function(){

   });

   $('.close-wind').click(function(){
      $(this).closest('.wind').hide();
   });


   $('input.date-style').will_pickdate({
      format: 'j F Y',
      inputOutputFormat: 'Y.m.d',
      days: ['Пн', 'Вт', 'Ср', 'Чт','Пт', 'Сб', 'Вс'],
      months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      timePicker: false,
      militaryTime: true,
      yearsPerPage:3,
      allowEmpty:true
   });


   resizer();

   prSlider();

   resizer();

   if ($('.shadow.top').length===0){
      $('.head').addClass('need-shadow');
   }

   $('.close-wind-button').click(function(ev){
      $(this).closest('.wind').hide();
   });


   $('.wind').click(function(ev){
      ev.stopImmediatePropagation();
   });

   $('.button-send').click(function(ev){
      ev.stopImmediatePropagation();

      var number = 0;
      if ($('#numberid')) {
         number = $('#numberid').attr('rel');
      }

      $.ajax({
         type: 'POST',
         url: '/remote.php?f=1&n='+number,
         data: $(this).closest('form').serialize(),
         success: function(data)
         {
            var pos = data.indexOf('success');
            if (pos > -1) {
               data = data.replace('success', '');
            }
            if (pos > -1) {
               $('.wind').hide();
               $('#form_ans').show();
               var he = 391/2;
               $('#form_ans').css({"margin-top":-he+"px"});
            } else {
               alert(data);
            }
         }
      });

      return false;
   });

   $('.bron-button, .but1').click(function(ev){
      ev.stopImmediatePropagation();
      $('#bron_wind').show();
      var he = 398/2;
      $('#bron_wind').css({"margin-top":-he+"px"});
   });

   $('.close-but').click(function(){
      $('.wind').hide();
      $('.gal').hide();
   });

   

   var op = $('.one-photo');
   for (var i=2;i<op.length;i+=3){
      op.eq(i).css({"margin-right":0});
   }
   if (op.length>0){
      $('h1').eq(0).css({"margin-bottom":"60px"});
   }

   $('.one-photo').click(function(){
      $('.to-gal').eq(parseInt($(this).attr("big"),10)).trigger("click");
      $('.gal').show();
   });

   $('.to-gal').click(function(){
      $('.to-bg').css({"background-image":"url('"+$(this).attr('openph')+"')"});
      $('.to-gal').removeClass("active");
      $(this).addClass("active");
      $('.title-photo').text($(this).attr("title"));
      recalcPaging($(this).closest('.paging'));
   });

   $('.gal-right').click(function(){
      var ac = $('.to-gal.active'),
         n = ac.next('.to-gal');
      if (n.length===0)
         n = $('.to-gal').eq(0);
      n.trigger("click");
   });

   $('.gal-left').click(function(){
      var ac = $('.to-gal.active'),
         n = ac.prev('.to-gal');
      if (n.length===0)
         n = $('.to-gal').eq($('.to-gal').length-1);
      n.trigger("click");
   });

   setTimeout(function(){op.addClass('gal-trans');},100);

   if ($('.numbers').length>0){
      $('.slider').css({"height":"600px"});
   }
   
   
   if ($('.light.absolute-bot').length>0){
      var c1 = $('.light.absolute-bot').closest('.column'),
         c2 = c1.next('.column'),
         h1 = c1.height(),
         max = h1,
         h2 = c2.height();
         
       if (h2>max)
         max = h2;
      
      c1.height(max);
      c2.height(max);
   }
   createInterval();
});

function recalcPaging(pag){
   var els = pag.find('.to-gal'),
      act = pag.find('.actvie'),
      current = 0,
      count = els.length;

   pag.find('.points').remove();
   for (var i=0;i<count;i++){
      if (els.eq(i).hasClass('active')){
         current = i;
         break;
      }
   }

   els.hide();

   show10(els, current);

   show10(els, count-current);

   if (els.eq(0).css("display")==="none"){
      pag.find('.gal-left').after(getPoints());
   }

   if (els.eq(count-1).css("display")==="none"){
      pag.find('.gal-right').before(getPoints());
   }

   var fv = 0;
   var k;

   for (k=0;k<count;k++){
      if (els.eq(k).css("display")!=="none"){
         fv = k;
         break;
      }
   }

   var anv = -1;

   for (k=fv;k<count;k++){
      if (els.eq(k).css("display")==="none"){
         anv = k;
         break;
      }
   }

   var sv = -1;

   for (k=anv;k<count;k++){
      if (els.eq(k).css("display")!=="none"){
         sv = k;
         break;
      }
   }

   if (fv>=0 && anv>0 && sv>0){
      els.eq(anv).after(getPoints());
   }
}

function getPoints(){
   return $("<span class='points'>...</span>");
}

function show10(els, start){
   var countShowed = 0,
      fl = 1;


   for (var i=0;i<12;i++) {
      var t = start + fl*i;

      var el = els.eq(t);
      if (t>=0 && el.length>0) {
         el.show();
         countShowed++;

         fl*=-1;
         t = start + fl*i;
         el = els.eq(t);
         if (t>=0 && el.length>0) {
            el.show();
            countShowed++;
         }

      } else {
         fl*=-1;
         t = start + fl*i;
         el = els.eq(t);
         if (t>=0 && el.length>0) {
            el.show();
            countShowed++;
         }
      }

      if (countShowed===12)
         break;

   }

   return countShowed;
}

var interSl;

function createInterval(){
   try{
      clearInterval(interSl);
   }catch(e){}

   interSl = setInterval(function(){
      $('.right-arr').trigger("click");
   }, 5000);
}

function prSlider(){
   var slider = $('.slider');
   for (var i=0;i<slider.length;i++){
      var s = slider.eq(i),
         h = "",
         ps = s.find('.photo-slide'),
         img = s.find('.fl');

      if (img.length>0) {
         for (var k = 0; k < img.length; k++) {
            s.find('.slide').append($(createSlide(img.eq(k), k + 1)));
            s.find('.right-arr').before($(createNum(img.eq(k), k + 1)));
         }
      }
      if (img.length===1){
         s.find('.right-arr').hide();
         s.find('.big-right-arr').hide();
         s.find('.left-arr').hide();
         s.find('.big-left-arr').hide();
         s.find('.one-num').hide();
      }

      if (s.find('.big-left-arr2').length>0){
         s.find('.right-arr').hide();
         s.find('.big-right-arr').hide();
         s.find('.left-arr').hide();
         s.find('.big-left-arr').hide();
         s.find('.one-num').hide();
      }

      if (ps.length>0) {

         if (s.find('.description-project').length>0){
            s.find('.right-arr').before($(createNum2(s.find('.description-project'), 1)));
         }

         for (var t = 0; t < ps.length; t++) {
            s.find('.right-arr').before($(createNum2(ps.eq(t), t + 2)));
         }
      }


      addClick(s);
      setTimeout(function(){
         s.find('.one-num').eq(0).trigger("click");
      }, 10);

   }
}

function createNum(img, i){
   return "<div class='one-num' big='"+img.attr("src")+"' alt='"+img.attr("alt")+"'></div>";
}

function createSlide(img, i){
   var im = new Image();
   im.src= img.attr("src");
   im.src= img.attr("src");
   return "<div class='sl slide_is_"+i+"' style=\"background-image: url('"+img.attr("src")+ "'); opacity: 0;\"><div class='text-on-slide'>"+img.attr("alt")+"</div></div>";
}

function createNum2(img, i){
   return "<div class='one-num type-new' bigid='"+img.attr("id")+"' ></div>";
}

function addClick(slider){
   slider.find('.one-num').click(function(ev){
      ev.stopImmediatePropagation();
      createInterval();
      var s = $(this).closest(".slider");

      s.find('.one-num').removeClass("active");
      $(this).addClass("active");
      if (!$(this).hasClass("type-new")) {
         //s.find('.slide').css({"background-image": "url('" + $(this).attr('big') + "')"});
         s.find('.slide').toggleClass("transform-t");
         //s.find('.text-on-slide').html($(this).attr('alt'));
         s.find('.slide .sl').stop().animate({"opacity":0}, 1000);
         s.find('.slide_is_'+$(this).text()).stop().animate({"opacity":1}, 1000);
      } else {

         var el =  $('#'+$(this).attr('bigid')),
            l = el.offset().left,
            bw = $('body').width(),
            wid = el.width(),
            mar = l - bw/2 + wid/ 2,
            ml = parseInt(s.find('.slides').css("margin-left"),10) ;


         s.find('.slides').removeClass('sl-trans');
         var td = el.closest("td"),
            td2 = td,
            td3 = td,
            next = 0,
            prev = 0,
            table = el.closest("table");
         while(true){
            if (td2.next("td").length>0){
               td2 = td2.next("td");
               next++;
            } else {
               break;
            }
         }

         while(true){
            if (td3.prev("td").length>0){
               td3 = td3.prev("td");
               prev++;
            } else {
               break;
            }
         }

         var cp = parseInt((next-prev)/2,10);

         while (cp>0){
            var l = td2.prev("td");
            td3.before(td2);
            td3 = td3.prev("td");
            ml -= td2.width();
            td2 = l;
            cp--;
         }

         while (cp<0){
            var l = td3.next("td");
            td2.after(td3);
            td2 = td2.next("td");
            ml += td3.width();
            td3 = l;
            cp++;
         }
         var self = this;
         s.find('.slides').css({"margin-left":ml+"px"});
         setTimeout(function(){
            ml -= mar;
            s.find('.slides').addClass('sl-trans');
            s.find('.slides').css({"margin-left":ml+"px"});
            s.find('.slides .active').removeClass('active');
            el.addClass('active');
            var bw = $('body').width(),
               warr = (bw - el.width())/2;

            $('.big-left-arr, .big-right-arr').css({"width":warr+"px"});
         }, 20);


      }

   });
}

var bloked = 0;
var interb ;


function resizer(){
   setTimeout(function(){
      var w = $('body').width(),
         h = $(document).height();


   }, 1);
}

//document.onmousemove = mouseMove;

function mouseMove(event){
   event = fixEvent(event);
   var newX = event.pageX - $('.subscribe').offset().left;//-document.body.scrollTop;
   if (startX === -1)
      startX === newX;

   var mar = parseInt((newX -startX)/30,10);

   if (mar){

      $('.subscribe').css("background-position", (mar)+"px 0");

   }
}

function fixEvent(e) {
   // получить объект событие для IE
   e = e || window.event;

   // добавить pageX/pageY для IE
   if ( e.pageX == null && e.clientX != null ) {
      var html = document.documentElement;
      var body = document.body;
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
   }

   // добавить which для IE
   if (!e.which && e.button) {
      e.which = (e.button & 1) ? 1 : ( (e.button & 2) ? 3 : ( (e.button & 4) ? 2 : 0 ) );
   }

   return e;
};



